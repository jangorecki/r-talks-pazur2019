#!/bin/bash
set -e

# dplyr memory issue dplyr#4334, testing 0.8.1 and 0.7.8
Rscript -e 'devtools::install_version("dplyr", version="0.8.1", lib="./dplyr081", quiet=TRUE, repos="https://cloud.r-project.org")'
Rscript -e 'devtools::install_version("dplyr", version="0.7.8", lib="./dplyr078", quiet=TRUE, repos="https://cloud.r-project.org")'

# memory: `:=`
../cgmemtime/cgmemtime Rscript -e 'set.seed(108); DF=data.frame(x=rnorm(1e8)); DF[c("x2","x3","x4")] = list(DF$x^2, DF$x^3, DF$x^4)'
../cgmemtime/cgmemtime Rscript -e 'library(dplyr, lib.loc="dplyr081"); packageVersion("dplyr"); set.seed(108); TB=tibble(x=rnorm(1e8)); TB %>% mutate(x2 = x^2, x3 = x^3, x4 = x^4)'
../cgmemtime/cgmemtime Rscript -e 'library(dplyr, lib.loc="dplyr078"); packageVersion("dplyr"); set.seed(108); TB=tibble(x=rnorm(1e8)); TB %>% mutate(x2 = x^2, x3 = x^3, x4 = x^4)'
../cgmemtime/cgmemtime Rscript -e 'library(data.table); set.seed(108); DT=data.table(x=rnorm(1e8)); DT[, `:=`(x2 = x^2, x3 = x^3, x4 = x^4)]'
../cgmemtime/cgmemtime Rscript -e 'library(data.table); set.seed(108); DT=data.table(x=rnorm(1e8)); set(DT, j=c("x2","x3","x4"), value=list(DT$x^2, DT$x^3, DT$x^4))'
DF[c("x2","x3","x4")] = list(DF$x^2, DF$x^3, DF$x^4)
# 3.4 GB  # 23s
TB %>% mutate(x2 = x^2, x3 = x^3, x4 = x^4)
# 3.06 GB # 0.8.1
TB %>% mutate(x2 = x^2, x3 = x^3, x4 = x^4)
# 3.06 GB # 0.7.8
DT[, `:=`(x2 = x^2, x3 = x^3, x4 = x^4)]
# 5.29 GB
set(DT, j=c("x2","x3","x4"), value=list(DT$x^2, DT$x^3, DT$x^4))
# 3.04 GB

# memory: aggregate in place

# base R takes to much time
#../cgmemtime/cgmemtime Rscript -e 'set.seed(108); DF = data.frame(x=sample(1e8,, TRUE), z=rnorm(1e8)); do.call("rbind",lapply(split(DF, f=DF$x), function(df) {df$sum_z_by_x = sum(df$z); df}))'
# Error: Window function `SUM()` is not supported by this database
#../cgmemtime/cgmemtime Rscript -e 'library(dplyr); con=DBI::dbConnect(RSQLite::SQLite(), path="./sqlite.db"); copy_to(con, {set.seed(108); tibble(x=sample(1e8,, TRUE), z=rnorm(1e8))}, "aggregate_in_place", temporary=FALSE); TB = tbl(con, "aggregate_in_place"); TB %>% group_by(x) %>% mutate(sum_z_by_x = sum(z))'
#../cgmemtime/cgmemtime Rscript -e 'library(dplyr); con=DBI::dbConnect(RSQLite::SQLite(), path="./sqlite.db"); TB = tbl(con, "aggregate_in_place"); TB %>% group_by(x) %>% mutate(sum_z_by_x = sum(z))'

../cgmemtime/cgmemtime Rscript -e 'library(dplyr, lib.loc="dplyr081"); packageVersion("dplyr"); set.seed(108); TB = tibble(x=sample(1e8,, TRUE), z=rnorm(1e8)); TB %>% group_by(x) %>% mutate(sum_z_by_x = sum(z))'
../cgmemtime/cgmemtime Rscript -e 'library(dplyr, lib.loc="dplyr078"); packageVersion("dplyr"); set.seed(108); TB = tibble(x=sample(1e8,, TRUE), z=rnorm(1e8)); TB %>% group_by(x) %>% mutate(sum_z_by_x = sum(z))'
../cgmemtime/cgmemtime Rscript -e 'library(data.table); set.seed(108); DT = data.table(x=sample(1e8,, TRUE), z=rnorm(1e8)); DT[, sum_z_by_x := sum(z), by = x]'
TB %>% group_by(x) %>% mutate(sum_z_by_x = sum(z))
# 23.16 GB # 431s
TB %>% group_by(x) %>% mutate(sum_z_by_x = sum(z))
# 12.62 GB # 242s
DT[, sum_z_by_x := sum(z), by = x]
# 3.48 GB  # 64s

# memory: update on join

# incompatiblity of dependencies dbplyr requires dplyr 0.8
#../cgmemtime/cgmemtime Rscript -e 'library(dplyr, lib.loc="dplyr078"); library(dbplyr); packageVersion("dplyr"); con=DBI::dbConnect(RSQLite::SQLite(), "db.sqlite"); TB1 = tbl(con, "dt1"); TB2 = tbl(con, "dt2"); TB1 = TB1 %>% left_join(TB2, by = c("x","y")) %>% mutate(z = z * mul) %>% select(-mul) %>% compute()'

../cgmemtime/cgmemtime Rscript -e 'library(dplyr, lib.loc="dplyr081"); packageVersion("dplyr"); TB1=as_tibble(data.table::fread("dt1.csv", data.table=FALSE)); TB2=as_tibble(data.table::fread("dt2.csv", data.table=FALSE)); TB1 = TB1 %>% left_join(TB2, by = c("x","y")) %>% mutate(z = z * mul) %>% select(-mul)'
../cgmemtime/cgmemtime Rscript -e 'library(dplyr, lib.loc="dplyr078"); packageVersion("dplyr"); TB1=as_tibble(data.table::fread("dt1.csv", data.table=FALSE)); TB2=as_tibble(data.table::fread("dt2.csv", data.table=FALSE)); TB1 = TB1 %>% left_join(TB2, by = c("x","y")) %>% mutate(z = z * mul) %>% select(-mul)'
../cgmemtime/cgmemtime Rscript -e 'library(data.table); DT1=fread("dt1.csv"); DT2=fread("dt2.csv"); DT1[DT2, z := z * i.mul, on=.(x,y)]'
../cgmemtime/cgmemtime Rscript -e 'library(dplyr, lib.loc="dplyr081"); packageVersion("dplyr"); con=DBI::dbConnect(RSQLite::SQLite(), "db.sqlite"); TB1 = tbl(con, "dt1"); TB2 = tbl(con, "dt2"); TB1 = TB1 %>% left_join(TB2, by = c("x","y")) %>% mutate(z = z * mul) %>% select(-mul) %>% compute()'
TB1 = TB1 %>% left_join(TB2, by = c("x","y")) %>% mutate(z = z * mul) %>% select(-mul)
# 4.18 GB, 16s
TB1 = TB1 %>% left_join(TB2, by = c("x","y")) %>% mutate(z = z * mul) %>% select(-mul)
# 3.44 GB, 32s
DT1[DT2, z := z * i.mul, on=.(x,y)]
# 3.43 GB, 7s
TB1 = TB1 %>% left_join(TB2, by = c("x","y")) %>% mutate(z = z * mul) %>% select(-mul) %>% compute()
# 1.48 GB, 81s

# memory: aggregate during join
../cgmemtime/cgmemtime Rscript -e 'library(dplyr, lib.loc="dplyr081"); packageVersion("dplyr"); TB1=as_tibble(data.table::fread("dt1.csv", data.table=FALSE)); TB2=as_tibble(data.table::fread("dt2.csv", data.table=FALSE)); TB1 %>% group_by(x, y) %>% summarise(z = sum(z)) %>% right_join(TB2) %>% mutate(z = z * mul) %>% select(-mul)'
../cgmemtime/cgmemtime Rscript -e 'library(dplyr, lib.loc="dplyr078"); packageVersion("dplyr"); TB1=as_tibble(data.table::fread("dt1.csv", data.table=FALSE)); TB2=as_tibble(data.table::fread("dt2.csv", data.table=FALSE)); TB1 %>% group_by(x, y) %>% summarise(z = sum(z)) %>% right_join(TB2) %>% mutate(z = z * mul) %>% select(-mul)'
../cgmemtime/cgmemtime Rscript -e 'library(dplyr, lib.loc="dplyr081"); packageVersion("dplyr"); con=DBI::dbConnect(RSQLite::SQLite(), "db.sqlite"); TB1 = tbl(con, "dt1"); TB2 = tbl(con, "dt2"); TB2 %>% left_join(TB1 %>% group_by(x, y) %>% summarise(z = sum(z))) %>% mutate(z = z * mul) %>% select(-mul) %>% compute()'
##debug##../cgmemtime/cgmemtime Rscript -e 'library(data.table); DT1=fread("dt1.csv"); DT2=fread("dt2.csv"); DT1 = DT1[DT2, .(z = sum(z) * i.mul), by=.EACHI, on=.(x,y)]'
../cgmemtime/cgmemtime Rscript -e 'library(data.table); DT1=fread("dt1.csv"); DT2=fread("dt2.csv"); DT1[DT2, .(z = sum(z) * i.mul), by=.EACHI, on=.(x,y)]'
../cgmemtime/cgmemtime Rscript -e 'library(data.table); DT1=fread("dt1.csv"); DT2=fread("dt2.csv"); DT1[DT2, z := sum(z) * i.mul, by=.EACHI, on=.(x,y)]'
TB1 %>% group_by(x, y) %>% summarise(z = sum(z)) %>% right_join(TB2) %>% mutate(z = z * mul) %>% select(-mul)
# 2.32 GB # 12s
# 2.08 GB # 34s
# 0.99 GB # 93s
DT1[DT2, .(z = sum(z) * i.mul), by=.EACHI, on=.(x,y)]
# 2.21 GB #  4s
DT1[DT2, z := sum(z) * i.mul, by=.EACHI, on=.(x,y)]
# 2.21 GB #  5s

## old
#../cgmemtime/cgmemtime Rscript -e 'library(data.table); DT1=fread("dt1.csv"); DT2=fread("dt2.csv"); DT2[DT1, .(z = sum(i.z) * mul), by=.EACHI, on=.(x,y)]'
#../cgmemtime/cgmemtime Rscript -e 'library(data.table); DT1=fread("dt1.csv"); DT2=fread("dt2.csv"); DT1[DT2, on=.(x,y)][, .(z = sum(z) * mul[1L]), by=.(x,y)]'
#../cgmemtime/cgmemtime Rscript -e 'library(data.table); DT1=fread("dt1.csv"); DT2=fread("dt2.csv"); DT2[DT1, on=.(x,y)][, .(z = sum(z) * mul[1L]), by=.(x,y)]'

# memory: in-place aggregate during join (moved to upper slide)
#../cgmemtime/cgmemtime Rscript -e 'library(data.table); DT1=fread("dt1.csv"); DT2=fread("dt2.csv"); DT1[DT2, z := sum(z) * i.mul, by=.EACHI, on=.(x,y)]'
## old
#../cgmemtime/cgmemtime Rscript -e 'library(data.table); DT1=fread("dt1.csv"); DT2=fread("dt2.csv"); DT1=DT2[DT1, on=.(x,y)][, z := sum(z) * mul[1L], by=.(x,y)][, mul:=NULL]'
#../cgmemtime/cgmemtime Rscript -e 'library(data.table); DT1=fread("dt1.csv"); DT2=fread("dt2.csv"); DT1=DT2[DT1, on=.(x,y)][, .(z = sum(z) * mul[1L]), by=.(x,y)][DT1, on=.(x,y), .(x,y,z)]'

# memory: aggregate during non-equi join

../cgmemtime/cgmemtime Rscript -e 'library(data.table); DT1=fread("dt1.csv"); DT2=fread("dt2.csv"); DT1[DT2, .N, on=.(x, y <= y)]'
../cgmemtime/cgmemtime Rscript -e 'library(data.table); DT1=fread("dt1.csv"); DT2=fread("dt2.csv"); DT1[DT2, .N, on=.(x, y <= y), by=.EACHI]'
../cgmemtime/cgmemtime Rscript -e 'library(dplyr, lib.loc="dplyr081"); packageVersion("dplyr"); TB1=as_tibble(data.table::fread("dt1.csv", data.table=FALSE)); TB2=as_tibble(data.table::fread("dt2.csv", data.table=FALSE)); TB1 %>% right_join(TB2, by="x") %>% filter(y.x <= y.y)'
../cgmemtime/cgmemtime Rscript -e 'library(dplyr, lib.loc="dplyr078"); packageVersion("dplyr"); TB1=as_tibble(data.table::fread("dt1.csv", data.table=FALSE)); TB2=as_tibble(data.table::fread("dt2.csv", data.table=FALSE)); TB1 %>% right_join(TB2, by="x") %>% filter(y.x <= y.y)'
../cgmemtime/cgmemtime Rscript -e 'library(DBI); con=dbConnect(RSQLite::SQLite(), "db.sqlite"); dbGetQuery(con, "SELECT count(*) FROM dt1 t1 JOIN dt2 t2 ON t1.x = t2.x AND t1.y <= t2.y")'
dbGetQuery(con, "SELECT count(*) FROM dt1 t1 JOIN dt2 t2 ON t1.x = t2.x AND t1.y <= t2.y")
# 0.06 GB, 105 min
DT1[DT2, .N, on=.(x, y <= y)]
Error in vecseq... more than 2^31 rows... try by=.EACHI
# 2.4 GB
DT1[DT2, .N, on=.(x, y <= y), by=.EACHI]
# 2.4 GB, ?? min 
TB1 %>% right_join(TB2, by="x") %>% filter(y.x <= y.y)
Error: std::bad_alloc
# 62.7 GB # 0.8.1, 0.7.8

../cgmemtime/cgmemtime Rscript -e 'library(data.table); DF1=fread("dt1.csv", data.table=FALSE); DF2=fread("dt2.csv", data.table=FALSE); ans = merge(DF1, DF2, on="x", all.y=TRUE); nrow(ans)'
#TODO
library(data.table); DF1=fread("dt1.csv", data.table=FALSE); DF2=fread("dt2.csv", data.table=FALSE);
ans = merge(DF1, DF2, by="x", all.y=TRUE)
nrow(ans[ans$y.x<=ans$y.y,])


# memory: ...

../cgmemtime/cgmemtime Rscript -e 'library(data.table); DT=fread("dt5.csv"); setkey(DT, a, b, c)'
../cgmemtime/cgmemtime Rscript -e 'library(data.table); DT=fread("dt5.csv"); setorder(DT, a, b, c)'
../cgmemtime/cgmemtime Rscript -e 'library(data.table); DT=fread("dt5.csv"); DT = DT[order(a, b, c)]'

../cgmemtime/cgmemtime Rscript -e 'N=1e8; DF=data.table::fread("dt5.csv", data.table=FALSE); DF = DF[with(DF, order(a, b, c)),] '
../cgmemtime/cgmemtime Rscript -e 'library(dplyr, lib.loc="dplyr081"); packageVersion("dplyr"); TB=as_tibble(data.table::fread("dt5.csv", data.table=FALSE)); TB = TB %>% arrange(a, b, c)'
../cgmemtime/cgmemtime Rscript -e 'library(dplyr, lib.loc="dplyr078"); packageVersion("dplyr"); TB=as_tibble(data.table::fread("dt5.csv", data.table=FALSE)); TB = TB %>% arrange(a, b, c)'

DF = DF[with(DF, order(a, b, c)),]
# 40.7 GB
TB = TB %>% arrange(a, b, c)
# 39.33 GB

# setDT

../cgmemtime/cgmemtime Rscript -e 'library(data.table); DF=fread("dt5.csv", data.table=FALSE); setDT(DF)'
../cgmemtime/cgmemtime Rscript -e 'library(data.table); DF=fread("dt5.csv", data.table=FALSE); DF=as.data.table(DF)'

../cgmemtime/cgmemtime Rscript -e 'library(data.table); DT=fread("dt5na.csv"); setnafill(DT, fill=0)'
../cgmemtime/cgmemtime Rscript -e 'library(data.table); DT=fread("dt5na.csv"); DT[is.na(DT)] = 0'
../cgmemtime/cgmemtime Rscript -e 'library(data.table); DT=fread("dt5na.csv"); for (col in names(DT)) set(DT, which(is.na(DT[[col]])), col, 0)'
../cgmemtime/cgmemtime Rscript -e 'library(data.table); DT=fread("dt5na.csv"); for (col in names(DT)) set(DT, DT[eval(call("is.na",as.name(col))), which=TRUE], col, 0)'
../cgmemtime/cgmemtime Rscript -e 'library(data.table); DF=fread("dt5na.csv", data.table=FALSE); DF[is.na(DF)] = 0'
../cgmemtime/cgmemtime Rscript -e 'library(dplyr, lib.loc="dplyr081"); library(tidyr); packageVersion("dplyr"); TB=as_tibble(data.table::fread("dt5na.csv", data.table=FALSE)); TB = TB %>% replace_na(as.list(setNames(rep(0,ncol(TB)), names(TB))))'

format_mem(57280244, 44133296)

# memory: := update columns ---

../cgmemtime/cgmemtime Rscript -e 'set.seed(108); DF=data.frame(x=rnorm(1e8)); idx = sample(1e8L, 1e2L); DF[idx, "x"] = NA_integer_'
../cgmemtime/cgmemtime Rscript -e 'library(dplyr, lib.loc="dplyr081"); packageVersion("dplyr"); set.seed(108); TB=tibble(x=rnorm(1e8)); idx = sample(1e8L, 1e2L); TB %>% mutate(x = if_else(row_number() %in% idx, NA_integer_, x))'
../cgmemtime/cgmemtime Rscript -e 'library(dplyr, lib.loc="dplyr078"); packageVersion("dplyr"); set.seed(108); TB=tibble(x=rnorm(1e8)); idx = sample(1e8L, 1e2L); TB %>% mutate(x = if_else(row_number() %in% idx, NA_integer_, x))'
../cgmemtime/cgmemtime Rscript -e 'library(data.table); set.seed(108); DT=data.table(x=rnorm(1e8)); idx = sample(1e8L, 1e2L); DT[idx, "x" := NA_integer_]'
../cgmemtime/cgmemtime Rscript -e 'library(data.table); set.seed(108); DT=data.table(x=rnorm(1e8)); idx = sample(1e8L, 1e2L); set(DT, idx, "x", NA_integer_)'
